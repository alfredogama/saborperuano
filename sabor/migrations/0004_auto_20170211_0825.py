# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sabor', '0003_food_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='EdgePairIngredient',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('shared_recipes_counter', models.PositiveIntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ListAdj',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ingredient_main_node', models.ForeignKey(to='sabor.IngredientName')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='edgepairingredient',
            name='ingredient_node1',
            field=models.ForeignKey(to='sabor.ListAdj'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='edgepairingredient',
            name='ingredient_node2',
            field=models.ForeignKey(to='sabor.IngredientName'),
            preserve_default=True,
        ),
    ]
