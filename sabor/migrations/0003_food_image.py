# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sabor', '0002_auto_20161119_0146'),
    ]

    operations = [
        migrations.AddField(
            model_name='food',
            name='image',
            field=sorl.thumbnail.fields.ImageField(upload_to=b'public', blank=True),
            preserve_default=True,
        ),
    ]
