from django.db import models
from sorl.thumbnail import ImageField


class Food(models.Model):
    name = models.CharField(max_length=100, blank=False)
    description = models.CharField(max_length=250, blank=True)
    preparation = models.TextField(blank=True)
    image = ImageField(upload_to='public', blank=True)

    def image_pic(self):
        if self.image:
            return """
                <img src="%s" width="100">
            """ % self.image.url
        else:
            return ""
    image_pic.allow_tags = True

    def __unicode__(self):
        return unicode(self.name)


class Ingredient(models.Model):
    name = models.CharField(max_length=100, blank=False, db_index=True)
    ingredient = models.ForeignKey(Food)
    ingredient_name = models.ForeignKey('IngredientName')
    quantity = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    unit = models.ForeignKey('Unit')

    def __unicode__(self):
        return unicode(self.name)


class IngredientName(models.Model):
    name = models.CharField(max_length=25)

    def __unicode__(self):
        return unicode(self.name)


class Unit(models.Model):
    name = models.CharField(max_length=25)

    def __unicode__(self):
        return unicode(self.name)
'''
  These two tables would be very useful if we were storing much more data
  which means these two guys allow us 'SCALABILITY', but this property becomes
  important when our data is above 10^5.
  
#-------------------------------------------------
# ListAdj : Adjacency List for pair-counting graph
#-------------------------------------------------

class ListAdj(models.Model):
   ingredient_main_node = models.ForeignKey(IngredientName)
   def __str__(self):
      return self.ingredient_main_node

#--------------------------------------------------
# EdgePairIngredient : Node for pair-counting graph
#--------------------------------------------------

class EdgePairIngredient(models.Model):
   ingredient_node1 = models.ForeignKey(ListAdj, on_delete=models.CASCADE)
   ingredient_node2 = models.ForeignKey(IngredientName)
   shared_recipes_counter = models.PositiveIntegerField(default=0)
   def __str__(self):
       return self.shared_recipes_counter
'''
