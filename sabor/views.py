from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from django.template import loader
from collections import defaultdict
import json
from models import *


def home(request):
    foods = Food.objects.all()
    return render(request, 'home.html', {'foods': foods})


def base(request):
    foods = Food.objects.all()
    data = []

    for item in foods:
        ingre = []
        for ingred in item.ingredient_set.all():
            ingre.append({'name_desc': ingred.name, 'ingradient_id': ingred.ingredient_name.id, 'ingradient_name': ingred.ingredient_name.name})
        data.append({'name': item.name, 'ingredient_detail': ingre})
    return HttpResponse(json.dumps(data), content_type='application/json')

def paircounting(request):
    ingredient_list = IngredientName.objects.order_by('name')
    context = {'ingredient_list': ingredient_list}
    return render(request, 'paircounting.html', context)


#-----------------------------------------------------------------------------------
# singlepair answer to the question How many recipes share (ingredient1, ingredient2)
#-----------------------------------------------------------------------------------

def singlepair(request):
    ingredient1 = request.GET['ingredient1']
    ingredient2 = request.GET['ingredient2']
    if(ingredient1.isdigit() and ingredient2.isdigit() and int(ingredient1)!= int(ingredient2)):
       cnt = 0
       foods = Food.objects.all()
       for item in foods:
          flag1 = False
          flag2 = False
          for ing in item.ingredient_set.all():
             if(int(ing.ingredient_name.id) == int(ingredient1)):flag1 = True
             if(int(ing.ingredient_name.id) == int(ingredient2)):flag2 = True
          if(flag1 == True and flag2 == True):cnt += 1
       node1 = IngredientName.objects.get(pk=ingredient1)
       node2 = IngredientName.objects.get(pk=ingredient2)
       data = []
       data.append({'node1':node1.name, 'node2':node2.name, 'count':cnt})
       return HttpResponse(json.dumps(data), content_type='application/json')
    else:
       ingredient_list = IngredientName.objects.order_by('name')
       context = {'ingredient_list': ingredient_list}
       return render(request, 'paircounting.html',context)

#------------------------------------------------------------------------------
# fullpair gives all pair of ingredients that appear at least once in a recipe
#------------------------------------------------------------------------------
def fullpair(request):
    adjlist = defaultdict(lambda: defaultdict(lambda: 0))
    foods = Food.objects.all()
    for item in foods:
        ingredients_list = []
        for ing in item.ingredient_set.all():
            ingredients_list.append(int(ing.ingredient_name.id))
        for i in range(0,len(ingredients_list)):
            for j in range(i+1,len(ingredients_list)):
                adjlist[ingredients_list[i]][ingredients_list[j]] += 1
                adjlist[ingredients_list[j]][ingredients_list[i]] += 1

    data = []
    set_ingredients = set()
    for i in range(1,len(adjlist)):
        for j in adjlist[i]:
          if((i,j) in set_ingredients):continue
          if((j,i) in set_ingredients):continue
          set_ingredients.add((i,j))
          ing1 = IngredientName.objects.get(pk=i).name
          ing2 = IngredientName.objects.get(pk=j).name
          data.append({'node1':ing1, 'node2':ing2, 'count':adjlist[i][j]})
    return HttpResponse(json.dumps(data), content_type='application/json')
