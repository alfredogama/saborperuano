from django.contrib import admin
from models import *


class InlineIngredient(admin.TabularInline):
    model = Ingredient
    fields = ('name', 'ingredient', 'ingredient_name', 'quantity', 'unit',)
    extra = 3


class FoodAdmin(admin.ModelAdmin):
    inlines = [InlineIngredient, ]
    list_display = ('name', 'image', 'image_pic',)
    readonly_fields = ('image_pic',)

admin.site.register(Ingredient)
admin.site.register(Food, FoodAdmin)
admin.site.register(Unit)
admin.site.register(IngredientName)

