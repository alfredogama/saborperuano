from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.paircounting, name='paircounting'),
    url(r'^singlepair/$', views.singlepair, name='singlepair'),
    url(r'^fullpair/$', views.fullpair, name='fullpair')
]
